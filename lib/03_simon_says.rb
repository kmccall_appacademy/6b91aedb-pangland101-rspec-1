def echo (input)
  input
end

def shout (input)
  input.upcase
end

def repeat (word, count=2)
  word + " #{word}"*(count-1)
end

def start_of_word (word, count)
  word[0...count]
end

def first_word (input)
  input.split[0]
end

def titleize (input)
  sentence = input.split
  sentence.each_with_index do |word, i|
    word[0] = word[0].upcase if word.length>4||i==0||i==sentence.length-1
  end.join(" ")
end
