def add (num1, num2)
  num1 + num2
end

def subtract (num1, num2)
  num1 - num2
end

def sum (arr)
  arr.length > 0 ? arr.reduce(:+) : 0
end

def multiply (arr)
  arr.length > 0 ? arr.reduce(:*) : 0
end

def power (num1, num2)
  num1 ** num2
end

def factorial (num)
  return 1 if num == 0
  return 1 if num == 1
  num * factorial(num - 1)
end
